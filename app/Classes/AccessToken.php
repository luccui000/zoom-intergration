<?php

namespace App\Classes;

use Illuminate\Contracts\Support\Arrayable;

class AccessToken implements Arrayable
{
    public string $accessToken;
    public string $tokenType;
    public int $expiresIn;
    public array $scopes;
    public function __construct($params) {
        $this->accessToken = data_get($params, 'access_token');
        $this->tokenType = data_get($params, 'token_type');
        $this->expiresIn = data_get($params, 'expires_in');
        $this->scopes = explode(' ', data_get($params, 'scope'));
    }

    public function toArray(): array
    {
        return [
            'access_token' => $this->accessToken,
            'token_type' => $this->tokenType,
            'expires_in' => $this->expiresIn,
            'scope' => $this->scopes,
        ];
    }
}
