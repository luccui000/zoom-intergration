<?php

namespace App\Http\Requests\Meeting;

use App\Enums\Meeting\MeetingTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'type' => ['nullable', new Enum(MeetingTypeEnum::class)],
            'page_size' => 'nullable|min:0|max:300',
            'next_page_token' => 'nullable|string',
            'page_number' => 'nullable|integer',
            'from' => 'nullable|date',
            'to' => 'nullable|date',
        ];
    }
}
