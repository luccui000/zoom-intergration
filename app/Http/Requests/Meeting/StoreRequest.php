<?php

namespace App\Http\Requests\Meeting;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            "topic" => "nullable|string",
            "type" => "nullable|integer|in:1,2,3,8",
            "start_time" => "nullable|date",
            "duration" => "nullable|integer",
            "timezone" => "nullable|string",
            "password" => "nullable|string|max:10",
            "schedule_for" => "nullable|string",
            "agenda" => "nullable|string|max:2000",
            "settings.*.host_video" => "nullable|boolean",
            "settings.*.participant_video" => "nullable|boolean",
            "settings.*.cn_meeting" => "nullable|boolean",
            "settings.*.in_meeting" => "nullable|boolean",
            "settings.*.join_before_host" => "nullable|boolean",
            "settings.*.mute_upon_entry" => "nullable|boolean",
            "settings.*.watermark" => "nullable|boolean",
            "settings.*.use_pmi" => "nullable|boolean",
            "settings.*.approval_type" => "nullable|in:0,1,2",
            "settings.*.registration_type" => "nullable|in:1,2,3",
            "settings.*.audio" => "nullable|in:both,telephony,voip",
            "settings.*.auto_recording" => "nullable|in:local,cloud,none",
            "settings.*.enforce_login" => "nullable|boolean",
            "settings.*.enforce_login_domains" => "nullable|string",
            "settings.*.alternative_hosts" => "nullable|string",
            "settings.*.close_registration" => "nullable|boolean",
            "settings.*.waiting_room" => "nullable|boolean",
            "settings.*.contact_name" => "nullable|string",
            "settings.*.contact_email" => "nullable|string",
            "settings.*.registrants_confirmation_email" => "nullable|boolean",
            "settings.*.registrants_email_notification" => "nullable|boolean",
            "settings.*.meeting_authentication" => "nullable|boolean",
            "settings.*.authentication_option" => "nullable|string",
            "settings.*.authentication_domains" => "nullable|string",
            "settings.*.authentication_name" => "nullable|string",
            "settings.*.global_dial_in_countries.*.city" => "nullable|string",
            "settings.*.global_dial_in_countries.*.country" => "nullable|string",
            "settings.*.global_dial_in_countries.*.country_name" => "nullable|string",
            "settings.*.global_dial_in_countries.*.number" => "nullable|string",
            "settings.*.global_dial_in_countries.*.type" => "nullable|in:toll,tollfree",
            "recurrence.*.type" => "nullable|integer|in:1,2,3",
            "recurrence.*.repeat_interval" => "nullable|integer",
            "recurrence.*.weekly_days" => "nullable|string",
            "recurrence.*.monthly_day" => "nullable|numeric|between:1,31",
            "recurrence.*.monthly_week" => "nullable|numeric|in:-1,1,2,3,4",
            "recurrence.*.monthly_week_day" => "nullable|integer|in:1,2,3,4,5,6,7",
            "recurrence.*.end_times" => "nullable|numeric|max:50|exclude_unless:end_date_time,null",
            "recurrence.*.end_date_time" => "nullable|date|exclude_unless:end_times,null",
            "tracking_fields.*.field" => "nullable|string",
            "tracking_fields.*.value" => "nullable|string",
        ];
    }
}
