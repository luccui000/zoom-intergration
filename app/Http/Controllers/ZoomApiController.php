<?php

namespace App\Http\Controllers;

use App\Http\Requests\Meeting\DeleteRequest;
use App\Http\Requests\Meeting\ListRequest;
use App\Http\Requests\Meeting\StoreRequest;
use App\Http\Requests\Meeting\UpdateRequest;
use App\Services\ZoomService;

class ZoomApiController extends Controller
{
    public function __construct(
        private readonly ZoomService $service
    )
    {
    }

    public function getAccessToken()
    {
        try {
            return response()->json([
                'access_token' => $this->service->getAccessToken()
            ]);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function zoomApiMeetingList(ListRequest $request)
    {
        try {
            $response = $this->service->listMeeting($request->all());
            return response()->json($response);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function zoomApiMeetingStore(StoreRequest $request)
    {
        try {
            $response = $this->service->createMeeting($request->all());
            return response()->json($response);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function zoomApiMeetingUpdate(UpdateRequest $request)
    {
        try {
            $meetingId = $request->get('meeting_id');
            $response = $this->service->updateMeeting($meetingId, $request->all());
            return response()->json($response);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function zoomApiMeetingDelete(DeleteRequest $request)
    {
        try {
            $meetingId = $request->get('meeting_id');
            $response = $this->service->deleteMeeting($meetingId);
            return response()->json($response);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
