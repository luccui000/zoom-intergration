<?php

namespace App\Enums;

enum ZoomApiPathEnum: string
{
    case CREATE_MEETING = '/users/me/meetings';
    case LIST_MEETING = '/users/me/meetings/';
}
