<?php

namespace App\Enums\Meeting;

enum MeetingTypeEnum: string
{
    case SCHEDULED = 'scheduled';
    case LIVE = 'live';
    case UPCOMING = 'upcoming';
    case UPCOMING_MEETINGS = 'upcoming_meetings';
    case PREVIOUS_MEETINGS = 'previous_meetings';
}
