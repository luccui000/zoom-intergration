<?php

namespace App\Contracts;

interface MeetingInterface
{
    public function createMeeting($payloads);
    public function listMeeting($request);
}
