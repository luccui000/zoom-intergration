<?php

namespace App\Services;

use App\Classes\AccessToken;
use App\Contracts\MeetingInterface;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class ZoomService implements MeetingInterface
{
    private string $baseApi;
    private array $credentials;
    const ZOOM_APIS = [
        'LIST_MEETING' => '/users/me/meetings',
        'CREATE_MEETING' => '/users/me/meetings',
        'UPDATE_MEETING' => '/meetings/',
        'DELETE_MEETING' => '/meetings/',
    ];

    public function __construct(array $configs = [])
    {
        $serviceConfigs = config('services.schedule.zoom');
        $this->credentials = array_merge($serviceConfigs, $configs);
        $this->baseApi = data_get($this->credentials, 'base_api');
    }

    /**
     * @throws \Exception
     */
    public function listMeeting($request)
    {
        $response =  Http::withToken($this->getAccessToken())
            ->get($this->getZoomApi(self::ZOOM_APIS['LIST_MEETING']), $request);

        if(!$response->ok()) {
            throw new \Exception('Something went wrong');
        }

        return json_decode($response->body());
    }

    /**
     * @throws \Exception
     */
    public function createMeeting($payloads)
    {
        $response =  Http::withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ])
            ->withToken($this->getAccessToken())
            ->post($this->getZoomApi(self::ZOOM_APIS['CREATE_MEETING']), $payloads);

        if($response->status() != Response::HTTP_CREATED) {
            throw new \Exception('Something went wrong');
        }

        return json_decode($response->body());
    }

    /**
     * @throws \Exception
     */
    public function updateMeeting($meetingId, array $payloads)
    {
        if(isset($payloads['meeting_id'])) {
            unset($payloads['meeting_id']);
        }

        $response =  Http::withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ])
            ->withToken($this->getAccessToken())
            ->patch($this->getZoomApi(self::ZOOM_APIS['UPDATE_MEETING'] . $meetingId), $payloads);

        if($response->status() != Response::HTTP_NO_CONTENT) {
            throw new \Exception('Something went wrong');
        }

        return json_decode($response->body());
    }

    /**
     * @throws \Exception
     */
    public function deleteMeeting($meetingId)
    {
        $response =  Http::withToken($this->getAccessToken())
            ->delete($this->getZoomApi(self::ZOOM_APIS['DELETE_MEETING'] . $meetingId));

        if($response->status() != Response::HTTP_NO_CONTENT) {
            throw new \Exception('Something went wrong');
        }

        return json_decode($response->body());
    }

    /**
     * @throws \Exception
     */
    public function getAccessToken(): string
    {
        $accessTokenUrl = data_get($this->credentials, 'access_token_url');
        $clientId = data_get($this->credentials, 'client_id');
        $clientSecret = data_get($this->credentials, 'client_secret');
        $accountId = data_get($this->credentials, 'account_id');

        $encodeAuthorization = base64_encode("$clientId:$clientSecret");

        $response = Http::asForm()
            ->withHeaders([
                'Authorization' => "Basic $encodeAuthorization"
            ])
            ->post($accessTokenUrl, [
                'grant_type' => 'account_credentials',
                'account_id' => $accountId,
            ]);

        if(!$response->ok()) {
            throw new \Exception('Can not create request');
        }

        $jsonResponse = new AccessToken(json_decode($response->body()));

        return $jsonResponse->accessToken;
    }

    public function getZoomApi($path): string
    {
        return $this->baseApi . $path;
    }

}
