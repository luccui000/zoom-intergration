<?php

use App\Http\Controllers\ZoomApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::post('/zoom-api/get-token', [ZoomApiController::class, 'getAccessToken']);
Route::post('/zoom-api/meeting/list', [ZoomApiController::class, 'zoomApiMeetingList']);
Route::post('/zoom-api/meeting/store', [ZoomApiController::class, 'zoomApiMeetingStore']);
Route::post('/zoom-api/meeting/update', [ZoomApiController::class, 'zoomApiMeetingUpdate']);
Route::post('/zoom-api/meeting/delete', [ZoomApiController::class, 'zoomApiMeetingDelete']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
